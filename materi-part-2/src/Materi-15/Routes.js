import React, { useContext } from "react"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import {Layout} from "antd"

import IntroReactjsExample from '../Materi-9/IntroReactjsExample'
import HelloWorld from '../Materi-10/HelloWorld'
import UserInfo from '../Materi-10/UserInfo'
import Timer from '../Materi-11/Timer'
import Peserta from '../Materi-12/Peserta'
import HooksExample from '../Materi-13/HooksExample'
import HooksWithAxios from '../Materi-13/HooksWithAxios'
import Movie from "../Materi-14/Movie"
import Nav from "../Materi-16/Nav"
import ButtonExample from "../Materi-16/ButtonExample";
import Register from "../Materi-17/Register"
import Login from "../Materi-17/Login"
import PesertaWithAuth from "../Materi-17/PesertaWithAuth";
import { UserContext } from "../Materi-17/UserContext";
import ChangePassword from "../Materi-Tambahan/ChangePassword";


const {Content} = Layout

const Routes = () =>{
  const [user] = useContext(UserContext)

  const LoginRoute = ({...props}) =>{
    if (user === null){
      return <Route {...props} />
    }else{
      return <Redirect to="/" />
    }
  }

  // contoh route khusus yang login
  const PrivateRoute = ({...props}) =>{
    if (user){
      return <Route {...props} />
    }else{
      return <Redirect to="/login" />
    }
  }

  return (
    <Layout>
      <Router>
          <Nav/>
          {/* A <Switch> looks through its children <Route>s and
              renders the first one that matches the current URL. */}
          <Content style={{ padding: '0 50px', paddingTop: "10px" }}>
            <Switch>  
              <Route exact path="/">
                <IntroReactjsExample />
              </Route>
              <Route exact path="/materi-10-1">
                <HelloWorld />
              </Route>
              <Route exact path="/materi-11">
                <Timer start={4} />
              </Route>

              <Route exact path="/materi-10-2" component={UserInfo}/>
              <Route exact path="/materi-12" component={Peserta}/>
              <Route exact path="/materi-13-1" component={HooksExample}/>
              <Route exact path="/materi-13-2" component={HooksWithAxios}/>
              <Route exact path="/materi-14" component={Movie}/>
              <Route exact path="/materi-16" component={ButtonExample}/>
              <LoginRoute exact path="/register" component={Register}/>
              <LoginRoute exact path="/login" component={Login}/>
              <Route exact path="/peserta-with-auth" component={PesertaWithAuth}/>

              {/* ini contoh route khusus yang sudah login */}
              {/* <PrivateRoute exact path="/peserta-with-auth" component={PesertaWithAuth}/> */}
              <PrivateRoute exact path="/change-password" component={ChangePassword}/>
            </Switch>
          </Content>
      </Router>
    </Layout>
  )
}

export default Routes