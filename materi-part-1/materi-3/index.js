var sayHello = "Hello World!" 
console.log(sayHello)

var name = "John" // Tipe
var angka = 12
var todayIsFriday = false 

console.log(name) // "John"
console.log(angka) // 12
console.log(todayIsFriday) // false

var sentences = "Javascript" 
console.log(sentences[0]) // "J"
console.log(sentences[2]) // "v"

var word = "Saya Belajar Pemrograman"
console.log(word.length) // 24

var string1 = 'good';
var string2 = 'luck';
console.log(string1.concat(" "+string2)); // goodluck

var minimarketStatus = "open"
if ( minimarketStatus == "open" ) {
    console.log("saya akan membeli telur dan buah")
} else {
    console.log("minimarketnya tutup")
}

var angka = 8

if (angka === "8"){
  console.log("ini angka delapan tipe data string")
}else{
  console.log("ini angka delapan tipe data integer")
}

var buttonPushed = "mute";
switch(buttonPushed) {
  case "turnOff":   { console.log('matikan TV!'); break; }
  case "volumeDown":   { console.log('turunkan volume TV!'); break; }
  case "volumeUp":   { console.log('tingkatkan volume TV!'); break; }
  case "mute":   { console.log('matikan suara TV!'); break; }
  default:  { console.log('Tidak terjadi apa-apa'); }
}

// contoh if else if
var angka = 3;

if (angka % 2 === 0){
  console.log(angka + " adalah bilangan genap")
}else if (angka % 3 === 0){
  console.log(angka + " adalah bilangan ganjil dan habis di bagi tiga");
}else{
  console.log(angka + " adalah bilangan ganjil dan tidak habis di bagi tiga");
}